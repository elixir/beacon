# This base image builds upon python3, with pre-installed Python requirements and _node js_.
FROM jlebiodaunilu/elixirlu-basebeacon-python:v0.2.2
MAINTAINER Jacek Lebioda "jacek.lebioda@uni.lu"
# Prepare the structure and install pip
RUN mkdir -p /home/beacon/elixir-beacon \
             /home/beacon/elixir-beacon/code/frontend/ \
             /home/beacon/elixir-beacon/code/backend/beacon/templates \
             /home/beacon/elixir-beacon/code/backend/beacon/static && \
    pip3 install --default-timeout=60 -i https://pypi.lcsb.uni.lu/simple --upgrade pip
# Prepare python requirements; should be fast as they are already in the base image
COPY code/backend/requirements.txt /home/beacon/elixir-beacon/
WORKDIR /home/beacon/elixir-beacon
RUN pip3 install --default-timeout=60 -i https://pypi.lcsb.uni.lu/simple -r requirements.txt
# Build front-end files
COPY code/frontend/ /home/beacon/elixir-beacon/code/frontend/
WORKDIR /home/beacon/elixir-beacon/code/frontend
RUN yarn install && \
    node node_modules/gulp/bin/gulp.js
# Move the back-end files, and run it
COPY . /home/beacon/elixir-beacon
WORKDIR /home/beacon/elixir-beacon/code/backend
ENV BEACON_SETTINGS_YAML /home/beacon/elixir-beacon/configuration/beacon_information.yaml
ENV BEACON_DATASOURCES_YAML /home/beacon/elixir-beacon/configuration/connections.yaml
ENTRYPOINT ["gunicorn", "--bind", ":8123", "wsgi:app", "--access-logfile", "/log/access.log", "--error-logfile", "/log/error.log"]
EXPOSE 8123