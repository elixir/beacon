# Explanation
This folder contains front-end assets.
To compile them and move to destination folder, use `gulp` command inside this directory.
Note, that you HAVE TO do this after you have cloned the repository; otherwise the page will not work.
## Production
If you use docker, it is already handled (refer to `Dockerfile` and `.docker-compose.yml`).
If you don't use docker, you need to use `gulp` command every time front-end assets change. 

# Tutorial
## How to compile front-end assets?
```
(~)         $ cd elixir-beacon/code/frontend
(front-end) $ gulp
```

this should produce an output similar to:
```
[11:39:35] Using gulpfile ~\beacon\code\frontend\gulpfile.js
[11:39:35] Starting 'default'...
[11:39:35] Starting 'css'...
[11:39:35] Starting 'js'...
[11:39:35] Starting 'png'...
[11:39:35] Starting 'svg'...
[11:39:39] Finished 'css' after 3.54 s
[11:39:39] Finished 'js' after 3.54 s
[11:39:39] Finished 'svg' after 3.54 s
[11:39:39] Finished 'png' after 3.55 s
[11:39:39] Starting 'templates'...
[11:39:39] gulp-inject Nothing to inject into api_index.html.
[11:39:39] gulp-inject Nothing to inject into api_index.html.
[11:39:39] gulp-inject 1 file into index.html.
[11:39:39] gulp-inject 1 file into index.html.
[11:39:39] gulp-inject Nothing to inject into login.html.
[11:39:39] gulp-inject Nothing to inject into login.html.
[11:39:39] Finished 'templates' after 31 ms
[11:39:39] Finished 'default' after 3.59 s
```
