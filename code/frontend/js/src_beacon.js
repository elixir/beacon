$(document).ready(function() {
    var $query_form = $("#query_form");
    var $submit_button = $("#submit");

    var $datasetIds = $("#datasetIds");

    var $results_table = $("#results_table");
    var $results_table_body = $results_table.children("tbody");

    var $entry_template = $("#entry_template");
    var compiled_template = Handlebars.compile($entry_template.html());

    var $help_block = $(".help-block");

    var $error_template = $("#error_template");
    var compiled_error_template = Handlebars.compile($error_template.html());

    var $datasets_table = $("#datasets_table");
    var $datasets_table_body = $datasets_table.children("tbody")
    var $dataset_template = $("#dataset_template");
    var compiled_dataset_template = Handlebars.compile($dataset_template.html());

    var url = $("body").data("query_url");
    var metadata_url = $("body").data("metadata_url");

    var add_new_entry_to_the_table = function(response) {
        var new_entry_html = compiled_template(response);
        $results_table_body.append(new_entry_html);
    };

    var add_datasets = function(response) {
        $.each(response.datasets, function(key, value) {
            var option_id = value.id;
            var option_name = value.name;
            var option_description = value.description;
            $datasetIds.append($("<option></option>").attr("value", option_id).text(option_name).attr('tooltip', option_description));

            var new_dataset_html = compiled_dataset_template(value);
            $datasets_table_body.append(new_dataset_html);
        });
    }

    var show_error = function(response) {
        var error_html = compiled_error_template(response);
        $results_table_body.append(error_html);
    };

    var make_query = function() {
        var savedName = $datasetIds.attr('name');
        if ($datasetIds.val() == "all") {
            $datasetIds.removeAttr('name');
        }

        $.ajax({
            type: "GET",
            url: url,
            data: $query_form.serialize(),
            success: function(data) {
                if ('error' in data) {
                    show_error(data);
                } else {
                    add_new_entry_to_the_table(data);
                }
            }
        });

        $datasetIds.attr('name', savedName);
    };

    $submit_button.click(function(e) {
        make_query();
        e.preventDefault();
    });

    $.ajax({
        type: "GET",
        url: metadata_url,
        success: function(data) {
            if ('datasets' in data && 'length' in data['datasets']) {
                add_datasets(data);
            }
        }
    });
});