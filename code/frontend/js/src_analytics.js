/* These are the settings used ELIXIR Luxembourg; we use Matomo in the analytics */

window.settings = {
    expires: "180",
    matomoURL: "https://analytics.lcsb.uni.lu/hub/",
    siteID: "-1",
    accept_all_text: "Aggregate statistics cookies accepted",
    only_necessary_text: "Only necessary cookies accepted",
    doNotTrack_text: "Do Not Track is enabled",
    cookieName: "lap",
    bots: /bot|crawler|spider|crawling/i,
    timeout_hidebanner: "500",
    cookieDomain: "*"
};
