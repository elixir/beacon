// IMPORTING UTILITIES
var gulp = require('gulp'),
    gulp_uglify = require('gulp-uglify'),
    gulp_clean_css = require('gulp-clean-css'),
    gulp_concat = require('gulp-concat'),
    gulp_inject = require('gulp-inject'),
    merge_stream = require('merge-stream');

// CONSTS
var source_css_path = 'css/*.css',
    source_vendor_css_path = 'vendor/*.css',
    source_js_path = 'js/*.js',
    source_vendor_js_path = 'vendor/*.js',
    source_png_path = 'images/*.png',
    source_svg_path = 'images/*.svg',
    source_template_path = 'templates/*.html',
    destination_build_path = '../backend/beacon/static',
    destination_build_path_js = '../backend/beacon/static/*.js',
    destination_build_path_css = '../backend/beacon/static/*.css',
    destination_build_path_templates = '../backend/beacon/templates';

// TASKS
var task_css = gulp.task('css', function() {
    var own_css = gulp.src(source_css_path);
    var vendor_css = gulp.src(source_vendor_css_path);

    return merge_stream(vendor_css, own_css)
        .pipe(gulp_clean_css())
        .pipe(gulp_concat('build.css'))
        .pipe(gulp.dest(destination_build_path));
});

var task_png = gulp.task('png', function() {
    return gulp.src(source_png_path)
        .pipe(gulp.dest(destination_build_path));
});

var task_svg = gulp.task('svg', function() {
    return gulp.src(source_svg_path)
        .pipe(gulp.dest(destination_build_path));
});

var task_js = gulp.task('js', function() {
    return gulp.src([source_vendor_js_path, source_js_path])
        .pipe(gulp_uglify({compress: { hoist_funs: false }}))
        .pipe(gulp_concat('build.js'))
        .pipe(gulp.dest(destination_build_path));
});

var task_templates = gulp.task('templates', function() {
    return gulp.src(source_template_path)
        .pipe(gulp_inject(gulp.src(destination_build_path_js, {read:false}), {
            transform: function(file_path) {
                file_path = file_path.slice(file_path.lastIndexOf("/") + 1);
                return "<script src=\"{{ url_for('static', filename='" + file_path + "') }}\" crossorigin='anonymous' async></script>";
            }
        }))
        .pipe(gulp_inject(gulp.src(destination_build_path_css, {read:false}), {
            transform: function(file_path) {
                file_path = file_path.slice(file_path.lastIndexOf("/") + 1);
                return "<link rel='stylesheet' href=\"{{ url_for('static', filename='" + file_path + "') }}\" />";
            }
        }))
        .pipe(gulp.dest(destination_build_path_templates));
});

// DEFAULT RUNNER
gulp.task('default', gulp.series(
    gulp.parallel('css', 'js', 'png', 'svg'),
    'templates'
));