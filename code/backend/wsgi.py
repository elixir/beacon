#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os

# Sometimes, the Beacon package has to be added to a python path...
current_directory = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(current_directory, 'beacon'))

# ...so that the following import could work:
from beacon.server import create_app, get_path_to_configuration


config_path = get_path_to_configuration()
app = create_app(config_path)
