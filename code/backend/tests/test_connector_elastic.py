from unittest import TestCase
from beacon.connectors.adapters import ElasticSearchConnector


class ElasticConnectorSuite(TestCase):
    def test_get_id(self):
        connector = ElasticSearchConnector({})
        self.assertEqual(connector.get_id(),
                         "elastic_search",
                         "Should return correct name")
