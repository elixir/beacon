#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re

from os import remove
from unittest import TestCase
from uuid import uuid4
from yaml.scanner import ScannerError

from beacon.helpers import load_yaml_file
from beacon.helpers import test_input_with_regex
from beacon.helpers import truthy


class HelpersLoadYamlFileSuite(TestCase):
    @staticmethod
    def _generate_random_name():
        return f'tmp_{uuid4().hex}'

    @staticmethod
    def _create_file(filename, contents):
        """Create a temporary file"""
        with open(filename, 'w') as f:
            f.write(contents)

    def setUp(self):
        self.nonexistent_yaml = self._generate_random_name() + '123'
        self.correct_yaml = self._generate_random_name()
        self.incorrect_yaml = self._generate_random_name()
        self._create_file(self.correct_yaml, """---
# Test
correct_yaml:
    abc: ghi
    def: jkl
""")
        self._create_file(self.incorrect_yaml, """---
# Test
incorrect_yaml: blah
  abc: ghi:
-    def: jkl
""")

    def tearDown(self):
        remove(self.correct_yaml)
        remove(self.incorrect_yaml)

    def test_loading_correct_yaml(self):
        """Tests if yaml can be loaded correctly"""
        obj = load_yaml_file(self.correct_yaml)
        assert 'correct_yaml' in obj, \
            "YAML file should be loaded"
        assert 'abc' in obj['correct_yaml'], \
            "YAML file should contain nested structures"

    def test_loading_incorrect_yaml(self):
        """Tests if incorrect yaml will throw exceptions"""
        self.assertRaises(ScannerError,
                          load_yaml_file,
                          self.incorrect_yaml)

    def test_loading_nonexistent_file(self):
        """Tests if loading yaml that does not exist will fail"""
        self.assertRaises(FileNotFoundError,
                          load_yaml_file,
                          self.nonexistent_yaml)


class HelpersTestInputWithRegexSuite(TestCase):
    def test_test_input_with_regex(self):
        params = {
            'integer': '12314',
            'string': 'test string of letters and spaces',
            'null': None
        }
        self.assertTrue(
            test_input_with_regex(params, 'integer', re.compile(r'\d*')),
            "Regex against dict value containing integer"
        )
        self.assertTrue(
            test_input_with_regex(params, 'string', re.compile(r'[A-z\s]*')),
            "Regex against dict value containing string"
        )
        self.assertFalse(
            test_input_with_regex(params, 'not_there', re.compile(r'.*')),
            "Regex against dict value which is missing"
        )
        self.assertFalse(
            test_input_with_regex(params, 'string', re.compile(r'\d')),
            "Regex against dict value containing string, that should not match"
        )
        self.assertFalse(
            test_input_with_regex(params, 'null', re.compile('.*')),
            "Regex against dict value containing Null"
        )


class HelpersTestTruthy(TestCase):
    def test_truthy(self):
        self.assertTrue(truthy(1), 'should be true')
        self.assertTrue(truthy('1'), 'should be true')
        self.assertTrue(truthy('true'), 'should be true')
        self.assertTrue(truthy('True'), 'should be true')
        self.assertFalse(truthy('False'), 'should be false')
        self.assertFalse(truthy('0'), 'should be false')
        self.assertFalse(truthy(0), 'should be false')
