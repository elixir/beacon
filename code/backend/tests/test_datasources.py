#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from unittest import TestCase

from beacon.connectors.adapters import VariantDBConnector
from beacon.connectors.manager import ConnectorsManager
from beacon.datasources.manager import DataSourcesManager
from beacon.datasources.validators import DataSourcesValidator
from beacon.helpers import load_yaml_file


class DataSourcesManagerSuite(TestCase):
    def setUp(self):
        self.manager = DataSourcesManager()
        self.path_to_correct_empty_configuration = \
            "tests/fixtures/correct_empty_connections.yaml"
        self.path_to_correct_configuration = \
            "tests/fixtures/correct_connections.yaml"
        self.path_to_incorrect_configuration = \
            "tests/fixtures/incorrect_connections.yaml"

    def test_correct_configuration(self):
        path = self.path_to_correct_empty_configuration
        self.manager.load_data_sources_information_from_yaml_file(path)

    def test_incorrect_configuration(self):
        func = self.manager.load_data_sources_information_from_yaml_file
        path = self.path_to_incorrect_configuration
        self.assertRaises(ValueError,
                          func,
                          path)

    def test_instantiate_connector(self):
        # It should fail to instantiate something which is not AbstractConnector
        self.assertRaises(AttributeError,
                          self.manager.instantiate_connector,
                          dict, {})
        # But it should create a connector without any warnings
        new_one = self.manager.instantiate_connector(VariantDBConnector, {})
        self.assertTrue(isinstance(new_one, VariantDBConnector))

    def test_integration_with_connectors_manager(self):
        connectors_manager = ConnectorsManager()
        connectors_manager.discover_and_load_adapters()

        path = self.path_to_correct_configuration
        manager = DataSourcesManager(connectors_manager.connectors)
        manager.load_data_sources_information_from_yaml_file(path)

        assert len(manager.connectors) > 0


class DataSourcesValidatorSuite(TestCase):
    def setUp(self):
        self.validator = DataSourcesValidator()

    def test_correct_configuration(self):
        path = "tests/fixtures/correct_connections.yaml"
        correct_configuration = load_yaml_file(path)
        self.assertTrue(self.validator.validate(correct_configuration))

    def test_incorrect_configuration(self):
        path = "tests/fixtures/incorrect_connections.yaml"
        incorrect_configuration = load_yaml_file(path)
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect_configuration)

    def test_incorrect_configuration(self):
        path = "tests/fixtures/correct_connections.yaml"
        correct_configuration = load_yaml_file(path)
        incorrect_configuration = correct_configuration
        del incorrect_configuration['data_sources']
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect_configuration)
        del incorrect_configuration['datasets']
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect_configuration)

    def test_validate_dataset(self):
        self.assertRaises(ValueError,
                          self.validator.validate,
                          {})
        self.assertRaises(ValueError,
                          self.validator.validate,
                          {'id': '1'})
        self.assertRaises(ValueError,
                          self.validator.validate,
                          {'id': '1', 'active': '1'})
        self.assertRaises(ValueError,
                          self.validator.validate,
                          {'id': '1', 'active': '1', 'data_source': 'incorrect'})
