#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from unittest import TestCase

from beacon.connectors.manager import ConnectorsManager


class ConnectorsManagerSuite(TestCase):
    def setUp(self):
        self.manager = ConnectorsManager()

    def test_is_adapter_true(self):
        from beacon.connectors.adapters.variantDB import VariantDBConnector
        obj = VariantDBConnector({})
        self.assertTrue(self.manager.is_adapter(obj.__class__.__name__,
                                                obj.__class__),
                        "VariantDBConnector should be a correct adapter")

    def test_is_adapter_true_es(self):
        from beacon.connectors.adapters.elasticSearch import ElasticSearchConnector
        obj = ElasticSearchConnector({})
        self.assertTrue(self.manager.is_adapter(obj.__class__.__name__,
                                                obj.__class__),
                        "ElasticSearchConnector should be a correct adapter")

    def test_is_adapter_false(self):
        from beacon.connectors.adapters.abstract import AbstractConnector
        self.assertFalse(self.manager.is_adapter(AbstractConnector.__name__,
                                                 AbstractConnector.__class__),
                         "AbstractConnector should not be a correct adapter")

    def test_discover_and_load_adapters(self):
        adapters = self.manager.discover_and_load_adapters()
        self.assertEqual(len(adapters), 3, "it should load VariantDB, ES and mock adapters")
        assert 'variant_db' in adapters
