import json

from unittest import TestCase
from beacon.server import create_app, get_path_to_configuration


class End2endSuite(TestCase):
    @classmethod
    def setUpClass(cls):
        super(End2endSuite, cls).setUpClass()
        configuration_file = get_path_to_configuration()
        cls.app = create_app(configuration_file,
                             '../../configuration/templates/beacon_information.yaml',
                             '../../configuration/connections_ci.yaml')
        cls.client = cls.app.test_client()

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_beacon_metadata(self):
        response = self.client.get('/v1/beacon')
        self.assertEqual(response.status_code, 200)

        response_dict = json.loads(response.data)
        self.assertTrue('alternativeUrl' in response_dict)
        self.assertTrue('apiVersion' in response_dict)
        self.assertTrue('createDateTime' in response_dict)
        self.assertTrue('datasets' in response_dict)
        self.assertTrue('description' in response_dict)
        self.assertTrue('organization' in response_dict)
        self.assertTrue('version' in response_dict)

    def test_beacon_query(self):
        response = self.client.get('/v1/beacon/query?assemblyId=GRCh37&referenceName=1')
        self.assertEqual(response.status_code, 200)

    def test_beacon_queries_include_dataset_responses(self):
        def get_response(url):
            response = self.client.get(url)
            return json.loads(response.data)

        def assert_correct(url):
            self.assertTrue('error' not in get_response(url))

        def assert_incorrect(url):
            self.assertTrue('error' in get_response(url))

        base_url = '/v1/beacon/query?assemblyId=GRCh37&referenceBases=A&referenceName=1&includeDatasetResponses='

        assert_correct(base_url)
        assert_correct(base_url + 'MISS')
        assert_correct(base_url + 'NONE')
        assert_correct(base_url + 'ALL')
        assert_correct(base_url + 'hit')
        assert_incorrect(base_url + 'hits')
        assert_incorrect(base_url + 'ALLx')
        assert_incorrect(base_url + '1')
        assert_incorrect(base_url + 'true')
