from unittest import TestCase
from beacon.connectors.adapters import VariantDBConnector


class VariantDBConnectorSuite(TestCase):
    def test_get_id(self):
        connector = VariantDBConnector({})
        self.assertEqual(connector.get_id(), "variant_db", "Should return correct name")

    def test_translate_response(self):
        valid_true_response = '{"values":[5]}'
        valid_false_response = '{"values":[0]}'

        connector = VariantDBConnector({})
        self.assertTrue(connector.translate_response(valid_true_response),
                        "`translate_response` should return True for this string")
        self.assertFalse(connector.translate_response(valid_false_response),
                         "`translate_response` should return False for this string")
