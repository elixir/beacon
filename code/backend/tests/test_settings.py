#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from unittest import TestCase

from beacon.settings.settings import Settings
from beacon.settings.validators import BeaconInformationValidator


class SettingsSuite(TestCase):
    @staticmethod
    def test_load_beacon_information_from_yaml_file():
        path = "tests/fixtures/correct_beacon_information.yaml"
        s = Settings()
        s.load_beacon_information_from_yaml_file(path)
        assert 'beacon' in s.unparsed_settings
        assert 'datasets' in s.unparsed_settings['beacon']
        assert len(s.unparsed_settings['beacon']['datasets']) == 2

    def test_load_beacon_information_from_incorrect_yaml_file(self):
        path = "tests/fixtures/incorrect_beacon_information.yaml"
        s = Settings()
        self.assertRaises(Exception,
                          s.load_beacon_information_from_yaml_file,
                          path)

    def test_load_beacon_information_from_nonexistent_yaml_file(self):
        path = "tests/fixtures/nonexistent_beacon_information.yaml"
        s = Settings()
        self.assertRaises(Exception,
                          s.load_beacon_information_from_yaml_file,
                          path)


class SettingsValidatorSuite(TestCase):
    def setUp(self):
        self.validator = BeaconInformationValidator()

        # Correct
        self.correct_configuration_with_datasets = {
            "beacon": {
                "id": "1",
                "name": "name",
                "datasets": [
                    {'id': '1', 'name': 'name'},
                    {'id': '2', 'name': 'name2'}
                ]
            }
        }

        # Incorrect 1
        self.correct_configuration_without_datasets = deepcopy(
            self.correct_configuration_with_datasets
        )
        self.correct_configuration_without_datasets['beacon']['datasets'] = []

        # Incorrect 2
        self.correct_configuration_with_wrong_datasets = deepcopy(
            self.correct_configuration_with_datasets
        )
        del self.correct_configuration_with_wrong_datasets['beacon']['datasets'][0]['id']

        # Incorrect 3
        self.correct_configuration_with_wrong_datasets2 = deepcopy(
            self.correct_configuration_with_datasets
        )
        del self.correct_configuration_with_wrong_datasets2['beacon']['datasets'][0]['name']

    @staticmethod
    def _create_copy_without(obj, key1, key2):
        obj2 = deepcopy(obj)
        del obj2[key1][key2]
        return obj2

    def test_correct_configuration(self):
        self.assertTrue(self.validator.validate,
                        self.correct_configuration_without_datasets)
        self.assertTrue(self.validator.validate,
                        self.correct_configuration_with_datasets)

    def test_incorrect_configurations(self):
        self.assertRaises(ValueError,
                          self.validator.validate,
                          self.correct_configuration_with_wrong_datasets2)

        self.assertRaises(ValueError,
                          self.validator.validate,
                          self.correct_configuration_with_wrong_datasets)

        self.assertRaises(ValueError,
                          self.validator.validate,
                          {})

        incorrect = self._create_copy_without(
            self.correct_configuration_with_datasets,
            'beacon',
            'id'
        )
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect)

        incorrect = self._create_copy_without(
            self.correct_configuration_with_datasets,
            'beacon',
            'name'
        )
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect)

        incorrect = self._create_copy_without(
            self.correct_configuration_with_datasets,
            'beacon',
            'datasets'
        )
        self.assertRaises(ValueError,
                          self.validator.validate,
                          incorrect)
