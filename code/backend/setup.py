#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Beacon Package definition
"""

from setuptools import find_packages

try:
    from setuptools import setup
except ImportError:  # pragma: no cover
    from distutils.core import setup

import unittest


def get_test_suite():
    """Tries to get the tests"""
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern='*.py')
    return test_suite


setup(
    name='elixir_beacon',
    version='2.0.3',
    description="ELIXIR Beacon",
    author="Jacek Lebioda",
    author_email='jacek.lebioda@uni.lu',
    url='https://git-r3lab.uni.lu/elixir/elixir-beacon',
    packages=find_packages(exclude=['test*']),
    install_requires=[
        'Flask',
        'Flask-Cors',
        'flask-oidc',
        'gunicorn',
        'pyyaml',
        'requests'
    ],
    tests_require=[
        'Flask-Testing',
        'coverage'
    ],
    zip_safe=False,
    keywords=['beacon', 'lcsb', 'elixir'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Framework :: Flask',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='setup.get_test_suite',
)
