#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This file is just a helper to run Flask server on a developer's machine.
This code is not really intended to be run on production machine
(especially using development configuration).
For production code, refer to `wsgi.py`.
"""

import os

from server import create_app, get_path_to_configuration


def is_called_directly():
    return __name__ == '__main__'


def is_called_from_debug():
    return os.getenv('FLASK_DEBUG', '0') != '0' and __name__ == 'beacon.runner'


def should_run():
    return os.getenv('BEACON_RUN_SERVER', '0') != '0'


if is_called_directly() or is_called_from_debug():  # pragma: no cover
    app = create_app(get_path_to_configuration())
    if should_run():
        app.run()
