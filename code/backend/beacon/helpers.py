#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import logging
import urllib
import requests
import yaml

from typing import Pattern


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def load_yaml_file(path_to_yaml_file: str):
    """
    Reads a yaml file. Can throw an `yaml.YAMLError` exception.
    :param path_to_yaml_file:
    :return: a dictionary parsed from the file
    """
    try:
        with open(path_to_yaml_file, 'r') as stream:
            return yaml.safe_load(stream)
    except Exception as e:
        logger.error("YAML file not found: ")
        logger.error(e)
        raise e


def make_http_request(uri):
    """
    Gets the file identified by provided uri using HTTP GET request
    """
    return urllib.request.urlopen(uri).read().decode("utf-8")


def make_http_post_request(uri, data):
    """
    Makes a http POST request with the following data as JSON
    :param uri: URL
    :param data: the data to be sent as JSON
    :return: text contents of the resource
    """
    return requests.post(uri, json=data, verify=False).text


def test_input_with_regex(params: dict,
                          what: str,
                          regexp: Pattern):
    """
    Checks, if the supplied data are not null,
    and correct (whether they pass the regex)
    """
    return (what in params.keys()
            and params[what] is not None
            and regexp.match(params[what]))


def truthy(param):
    """
    Casts the parameter to boolean
    """
    if isinstance(param, str):
        return param.lower() in ('true', '1')
    return bool(param)
