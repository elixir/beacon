#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Functions, which create all the data types specified by Beacon's API v3.
The basic idea is to pass all the required values in **kwargs by their names.
"""

import copy
import re

from beacon.helpers import test_input_with_regex


class APIArgumentError(ValueError):
    """
    Exception thrown if there's something wrong with the argument
    provided to API query
    """
    pass


def create_beacon_dataset(**kwargs):
    """
    Creates an object which describes a dataset
    Needs the following keys in `**kwargs`:
      'id', 'name', 'assemblyId', 'createDateTime', 'updateDateTime'
    Optional keys in `**kwargs`:
      'description', 'version', 'variantCount', 'callCount', 'sampleCount'
      'externalUrl', 'info'
    """

    fields_required = ['id', 'name', 'assemblyId', 'createDateTime',
                       'updateDateTime']
    fields_optional = ['description', 'version', 'variantCount', 'callCount',
                       'sampleCount', 'externalUrl', 'info']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def validate_input(**kwargs):
    """
    Validates query parameters. If there's a violation,
    an `APIArgumentError` exception is raised
    """
    def wrap(name, message, regexp, optional=False):
        re_regexp = re.compile(regexp, re.IGNORECASE)
        if name not in kwargs and not optional:
            raise APIArgumentError("A required value is missing: {0}".format(name))
        if name in kwargs and kwargs[name] is not None and not re_regexp.match(kwargs[name]):
            arg = kwargs.get(name) if kwargs.get(name) is not None else '<<None>>'
            raise APIArgumentError("Invalid {0} ({1}) - {2}".format(name, arg, message))

    wrap('referenceName',
         'accepted ones are: 1-22, X, Y, M, MT',
         r'^([1-9]|1\d|2[012]|x|y|mt?)$')

    wrap('start',
         'should be a positive number',
         r'^\d*$',
         optional=True)

    wrap('startMin',
         'should be an empty string or a positive number',
         r'^\d*$',
         optional=True)

    wrap('startMax',
         'should be an empty string or a positive number',
         r'^\d*$',
         optional=True)

    wrap('end',
         'should be an empty string or a positive number',
         r'^\d*$',
         optional=True)

    wrap('endMin',
         'should be an empty string or a positive number',
         r'^\d*$',
         optional=True)

    wrap('endMax',
         'should be an empty string or a positive number',
         r'^\d*$',
         optional=True)

    wrap('assemblyId',
         'accepted ones are: GRCh38, GRCh37 and NCBI36',
         r'^(grch(38|37)|ncbi36)$')

    wrap('referenceBases',
         'should contain only A, C, T, G, D',
         r'^[actgd]*$')

    wrap('alternateBases',
         'should contain only A, C, T, G, D',
         r'^[actgd]*$',
         optional=True)

    wrap('includeDatasetResponses',
         'accepts only "ALL", "HIT", "MISS", "NONE" and empty string',
         r'^(hit|all|miss|none)?$',
         optional=True)

    return True


def inject_required(what_to_inject, from_what, to_what):
    """
    Extends the object `to_what` with `what_to_inject`.
    `what_to_inject` must be present in `from_what`.
    Throws an exception when the key is missing.
    Warning when dealing with mutable data structures
    (`inject_required` does not use deep copies).

    :param what_to_inject: The key of value to copy into `to_what`
    :param from_what: The dict which should contain `what_to_inject` key
    :param to_what: The output dict - the result will be here
    :return: nothing (to avoid confusion with `to_what` parameter)
    """

    if what_to_inject in from_what:
        to_what[what_to_inject] = from_what[what_to_inject]
    else:
        message = "`inject_required failed`, key {0} is marked as required" \
                  " and is not present in {1}".format(what_to_inject,
                                                      from_what)
        raise TypeError(message)
    return to_what


def inject_optional(what_to_inject, from_what, to_what):
    """
    Extends the object `to_what` with `what_to_inject`
    if `what_to_inject` is present in `from_what`.
    Warning when dealing with mutable data structures
    (`inject_optional` does not use deep copies).

    :param what_to_inject: The key of value to copy into `to_what`
    :param from_what: The dict which should contain `what_to_inject` key
    :param to_what: The output dict - the result will be here
    :return: nothing (to avoid confusion with `to_what` parameter)
    """

    if what_to_inject in from_what and from_what[what_to_inject] is not None:
        inject_required(what_to_inject, from_what, to_what)


def populate_fields(initial_dict, fields_required, fields_optional,
                    **kwargs):
    """
    Copies dict values (whose keys are in `fields_optional` or
    `fields_required` lists) from **kwargs into a copy
    of `initial_response`

    :param initial_dict: dict which will be populated with values
                         and returned
    :param fields_required: list of keys (strings) which must be present
                            in `**kwargs` and will be copied
    :param fields_optional: list of keys (strings) which can be present
                            in `**kwargs` and will be copied
    :param kwargs: dict of values to be used as a source
    :return: copy of `initial_response` dict extended by values
             from `**kwargs`
    """
    the_response = copy.deepcopy(initial_dict)

    for field_required in fields_required:
        inject_required(field_required, kwargs, the_response)

    for field_optional in fields_optional:
        inject_optional(field_optional, kwargs, the_response)

    return the_response


def create_organization(**kwargs):
    """
    Creates an object with metadata about organization which
    operates the beacon. Left as a reference.
    Needs the following keys in `**kwargs`:
      'id', 'name'
    Optional keys in `**kwargs`:
      'description', 'address', 'welcomeUrl', 'contactUrl', 'logoUrl', 'info'
    """

    fields_required = ['id', 'name']
    fields_optional = ['description', 'address', 'welcomeUrl', 'contactUrl',
                       'logoUrl', 'info']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon(**kwargs):
    """
    Creates an object with Beacon's metadata. Left as a reference.
    Needs the following keys in `**kwargs`:
      'id', 'name', 'apiVersion', 'organization'
    Optional keys in `**kwargs`:
      'description', 'version', 'welcomeUrl', 'info', 'alternativeUrl',
      'createDateTime', 'updateDateTime', 'datasets', 'sampleAlleleRequests'
    """

    fields_required = ['id', 'name', 'apiVersion', 'organization']
    fields_optional = ['description', 'version', 'welcomeUrl', 'info' 
                       'alternativeUrl', 'createDateTime', 'updateDateTime',
                       'datasets', 'sampleAlleleRequests']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_allele_request(**kwargs):
    """
    Creates an object to be used as a request
    Needs the following keys in `**kwargs`:
      'referenceName', 'start', 'referenceBases', 'alternateBases',
       'assemblyId'
    Optional keys in `**kwargs`:
      'datasetIds', 'includeDatasetResponses'
    """

    fields_required = ['referenceName']
    fields_optional = ['start', 'referenceBases',
                       'alternateBases', 'assemblyId',
                       'datasetIds', 'includeDatasetResponses',
                       'startMin', 'startMax',
                       'end', 'endMin', 'endMax']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_dataset_allele_response_error(**kwargs):
    """
    Creates an object with error dataset allele response data
    Needs the following keys in `**kwargs`:
      'datasetId', 'error'
    Optional keys in `**kwargs`:
      'exists', 'frequency', 'variantCount', 'callCount',
      'sampleCount', 'note', 'externalUrl', 'info'
    """

    fields_required = ['datasetId', 'error']
    fields_optional = ['exists', 'frequency', 'variantCount', 'callCount',
                       'sampleCount', 'note', 'externalUrl', 'info']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_dataset_allele_response_ok(**kwargs):
    """
    Creates an object with correct dataset allele response data
    Needs the following keys in `**kwargs`:
      'datasetId', 'exists'
    Optional keys in `**kwargs`:
      'error', 'frequency', 'variantCount', 'callCount',
      'sampleCount', 'note', 'externalUrl', 'info'
    """

    fields_required = ['datasetId', 'exists']
    fields_optional = ['error', 'frequency', 'variantCount', 'callCount',
                       'sampleCount', 'note', 'externalUrl', 'info']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_allele_response_ok(**kwargs):
    """
    Creates an object to be used as a correct query response
    Needs the following keys in `**kwargs`:
      'beaconId', 'exists'
    Optional keys in `**kwargs`:
      'error', 'alleleRequest', 'datasetAlleleResponses'
    """

    fields_required = ['beaconId', 'exists']
    fields_optional = ['error', 'alleleRequest', 'datasetAlleleResponses']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_allele_response_error(**kwargs):
    """
    Creates an object to be used as a incorrect query response
    Needs the following keys in `**kwargs`:
      'beaconId', 'error'
    Optional keys in `**kwargs`:
      'error', 'alleleRequest', 'datasetAlleleResponses'
    """

    fields_required = ['beaconId', 'error']
    fields_optional = ['error', 'alleleRequest', 'datasetAlleleResponses']

    return populate_fields({}, fields_required, fields_optional, **kwargs)


def create_beacon_error(**kwargs):
    """
    Creates an object used to inform about errors
    Optional keys in `**kwargs`:
      'message', 'errorCode'
    """
    error = {
        'errorCode': '400',
        'message': ''
    }

    fields_required = []
    fields_optional = ['message', 'errorCode']

    return populate_fields(error, fields_required, fields_optional, **kwargs)
