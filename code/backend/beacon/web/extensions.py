# -*- coding: utf-8 -*-

from flask_cors import CORS
from flask_oidc import OpenIDConnect
from flask_session import Session


def check_flask_session_configuration(configuration):
    """
    In order to start, Flask-Session require SESSION_TYPE to be present in the configuration
    """
    if 'SESSION_TYPE' not in configuration:
        raise ValueError("`SESSION_TYPE` not present in Flask Configuration")


cors = CORS()
oidc = OpenIDConnect()
flask_session = Session()
