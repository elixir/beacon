from datetime import datetime

from flask import jsonify, render_template, request, redirect, session
from flask_cors import cross_origin

from beacon.beacon import Beacon
from beacon.data_types import create_beacon_allele_request
from beacon.web.extensions import oidc

API_PREFIX = '/v1'


import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def get_current_year():
    year = datetime.now().year
    return str(year)


def register_views(app,
                   the_beacon: Beacon):
    """
    Injects all the used views into the application.
    Remember to have extensions initialized before calling.
    :param app: the Flask application
    :param beacon: the Beacon (in essence that's a back-end)
    :return: nothing (note - mutates app parameter)
    """

    @app.errorhandler(404)
    def error_404(_):
        """Just a simple 404 error"""
        return 'Not found', 404

    @app.context_processor
    def inject_values():
        """Will inject `is_logged_in` property to all templates"""
        current_year = '2020'
        values = {
            'is_logged_in': oidc.user_loggedin,
            'root': API_PREFIX,
            'current_year': get_current_year()
        }
        return values

    @app.after_request
    def disable_caching(the_response):
        """Sets response headers so that HTTP caching is disabled"""
        no_caching = 'no-cache, no-store, must-revalidate'
        the_response.headers["Cache-Control"] = no_caching
        the_response.headers["Expires"] = "0"
        the_response.headers["Pragma"] = "no-cache"
        return the_response

    # ========================================================================

    @app.route('/login', methods=['GET', 'POST'])
    @oidc.require_login
    def view_login():
        group_names = oidc.user_getfield("groupNames") or []
        session['permissions'] = group_names

        # In case anyone ever needs it
        user_information = {
            'sub': oidc.user_getfield("sub"),
            'name': oidc.user_getfield("name"),
            'preferred_username': oidc.user_getfield("preferred_username"),
            'email': oidc.user_getfield("email"),
            'bona_fide_status': oidc.user_getfield("bona_fide_status"),
            'groupNames': oidc.user_getfield("groupNames")
        }

        return render_template('index.html')

    @app.route('/logout', methods=['GET', 'POST'])
    def view_logout():
        session['permissions'] = []
        oidc.logout()
        # TODO: It'd be better not to use hardcoded URL here
        #       Maybe use autoconfiguration or client_secrets file instead?
        return redirect('https://login.elixir-czech.org/oidc/endsession')

    # ========================================================================

    @app.route('/', methods=['GET', 'POST'])
    def index():
        """Renders the index page of the web application"""
        return render_template('index.html')

    @app.route('/api_index', methods=['GET', 'POST'])
    def api_index():
        """Renders the index page of the api application"""
        return render_template('api_index.html')

    @cross_origin()
    @app.route(API_PREFIX + '/beacon',
               strict_slashes=False,
               methods=['GET', 'POST', 'OPTIONS'])
    def api_beacon():
        """`/beacon/` API endpoint"""
        user_session_data = session.get('permissions', [])
        beacon_information = the_beacon.get_information_for_user(user_session_data)
        return jsonify(beacon_information)

    @cross_origin()
    @app.route(API_PREFIX + '/beacon/query',
               methods=['GET', 'POST', 'OPTIONS'])
    def api_beacon_query():
        """`/beacon/query` API endpoint"""
        user_session_data = session.get('permissions', [])
        the_request = create_beacon_allele_request(**request.args.to_dict())
        the_response = the_beacon.dispatch_query(the_request, user_session_data)
        return jsonify(the_response)
