class BeaconInformationValidator:
    def validate(self, obj: dict):
        if 'beacon' not in obj:
            raise ValueError("`beacon` section missing")
        self._validate_beacon(obj['beacon'])

    def _validate_beacon(self, obj: dict):
        if 'id' not in obj:
            raise ValueError("`id` missing in `beacon`")
        if 'name' not in obj:
            raise ValueError("`name` missing in `beacon`")
        if 'datasets' not in obj:
            raise ValueError("`datasets` missing in `beacon`")
        for dataset in obj['datasets']:
            self._validate_dataset(dataset)

    @staticmethod
    def _validate_dataset(obj: dict):
        if 'id' not in obj:
            raise ValueError("`id` missing in `dataset`")
        if 'name' not in obj:
            raise ValueError("`name` missing in `beacon`")
