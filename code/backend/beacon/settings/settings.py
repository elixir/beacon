#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

from beacon.helpers import load_yaml_file
from .validators import BeaconInformationValidator

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Settings:
    def __init__(self):
        self.unparsed_settings = {}
        self.validator = BeaconInformationValidator()

    def load_beacon_information_from_yaml_file(self, path_to_yaml_file: str):
        self.unparsed_settings = load_yaml_file(path_to_yaml_file)
        self.validator.validate(self.unparsed_settings)

    def get_beacon_information(self):
        return self.unparsed_settings.get('beacon', {})
