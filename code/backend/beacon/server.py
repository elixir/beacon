# -*- coding: utf-8 -*-

"""
Implementation of Beacon Server using Flask

Initially based on (MIT licensed) Python Beacon Development Kit
(see https://github.com/mcupak/beacon-python/)
"""


import json
import logging
import os
import signal

from flask import Flask

from beacon.beacon import Beacon
from beacon.web import extensions
from beacon.web.views import register_views


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def register_extensions(app):
    """
    Connects the Flask application instance with the extensions used
    :param app: the Flask application
    :return: the Flask application with extensions initialized in the context
             of given application
    """
    logger.debug(' - Registering Flask Extensions...')
    logger.debug('    * registering CORS...')
    extensions.cors.init_app(app)
    logger.debug('    * registering CORS succeeded!')
    logger.debug('    * registering Flask-Session...')
    extensions.check_flask_session_configuration(app.config)
    try:
        extensions.flask_session.init_app(app)
    except ValueError:
        logger.debug('    * failed to register Flask-Session!')
        logger.debug('      Does your flask_settings.json contain `SESSION_TYPE` key?')
    logger.debug('    * registering Flask-Session succeeded!')
    logger.debug('    * registering OIDC...')
    try:
        extensions.oidc.init_app(app)
        # Being really verbose here, as the OIDC extension likes not to work
        # (usually because something is wrong with `client_secrets.json` file
        logger.debug('    * registering OIDC succeeded!')
    except KeyError as e:
        logger.error('    * failed to register OIDC extension!')
        logger.error('      No `OIDC_CLIENT_SECRETS` key in Flask''s configuration!')
        raise e
    except FileNotFoundError as e:
        logger.error('    * failed to register OIDC extension!')
        logger.error('      No `client_secrets.json` or wrong path! See `OIDC_CLIENT_SECRETS`')
        raise e
    except Exception as e:
        logger.error('    * failed to register OIDC extension!')
        logger.error('      Unspecified exception:')
        logger.error('      ' + str(e.__class__))
        raise e
    logger.debug('    * Extensions successfully registered!')


def create_app(path_to_config_file="",
               path_to_beacon_settings="",
               path_to_data_sources_settings=""):
    """
    Creates the Flask application based on config object (Flask)
    passed as an argument
    :param path_to_config_file: path to a JSON file used to configure Flask instance
    :param path_to_beacon_settings: optional, path to yaml file with beacon settings
    :param path_to_data_sources_settings: optional, path to yaml file with data sources
    :return: Flask application object
    """

    # The main Flask application
    logger.debug('Creating application:')

    logger.debug(' - Loading flask configuration...')
    config_object = load_configuration(path_to_config_file)

    logger.debug(' - Creating Flask app object...')
    app = Flask(__name__)
    app.config.from_mapping(config_object)
    register_extensions(app)

    # The Beacon
    logger.debug(' - Initializing Beacon...')
    beacon = Beacon()

    if len(path_to_data_sources_settings) and len(path_to_beacon_settings):
        logger.debug('    * will load the settings from manually specified files')
        init_result = beacon.initialize_from_yaml_files(path_to_beacon_settings, path_to_data_sources_settings)
    else:
        logger.debug('    * will load the settings from files specified in envvars')
        init_result = beacon.initialize_from_envvars()

    if init_result:
        logger.debug('    * success, Beacon correctly initialized!')
    else:
        logger.error('    * error, something went wrong, did you correctly set the envvars?')
        logger.error('      `BEACON_DATASOURCES_YAML` & BEACON_SETTINGS_YAML`')

    logger.debug(' - Registering views...')
    register_views(app, beacon)

    logger.debug(' - Mounting graceful-exit signal handlers...')
    signal.signal(signal.SIGINT, beacon.shutdown)
    signal.signal(signal.SIGTERM, beacon.shutdown)

    logger.debug(' - Flask application has been correctly loaded!')
    app.beacon = beacon
    return app


def load_configuration(path_to_config_file=""):
    try:
        logger.debug('   opening: ' + path_to_config_file)
        with open(path_to_config_file, 'r') as config_file:
            config_object = json.loads(config_file.read())
        if config_object.get('SUPPRESS_WARNINGS', False):
            logger.setLevel(logging.CRITICAL)
    except Exception as e:
        logger.error('Could not load Flask''s configuration!')
        if len(path_to_config_file) == 0:
            logger.error('You did not specify path to Flask''s configuration file')
        else:
            logger.error('Please check whether ' + path_to_config_file + ' is a valid JSON')
        raise e
    return config_object


def get_path_to_configuration():
    configuration_key = 'BEACON_CONFIGURATION_JSON'
    default_value = "../../configuration/flask_settings.json"
    return os.getenv(configuration_key, default_value)
