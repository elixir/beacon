from typing import List


class DataSourcesValidator:
    def __init__(self):
        self.valid_data_sources = []
        self.valid_connectors = []

    def reset(self):
        self.valid_data_sources = []
        self.valid_connectors = []

    def validate(self, obj: dict):
        self.reset()
        if 'connectors' not in obj:
            raise ValueError("`connectors` section missing")
        if 'data_sources' not in obj:
            raise ValueError("`data_sources` section missing")
        if 'datasets' not in obj:
            raise ValueError("`datasets` section missing")

        # Warning: the order matters here
        self._register_connectors(obj['connectors'])
        self._register_and_validate_data_sources(obj['data_sources'])
        self._validate_datasets(obj['datasets'])
        return True

    def _register_connectors(self, obj: List[dict]):
        for connector in obj:
            self.valid_connectors.append(connector)

    def _register_and_validate_data_sources(self, obj: List[dict]):
        for data_source in obj:
            self._register_and_validate_data_source(data_source)

    def _register_and_validate_data_source(self, obj: dict):
        if 'type' not in obj:
            raise ValueError("`type` section missing")
        if 'id' not in obj:
            raise ValueError("`id` section missing")
        if obj['type'] not in self.valid_connectors:
            raise ValueError("Using unrecognized connector!")
        self.valid_data_sources.append(obj['id'])

    def _validate_datasets(self, obj: List[dict]):
        for data_set in obj:
            self._validate_dataset(data_set)

    def _validate_dataset(self, obj: dict):
        if 'id' not in obj:
            raise ValueError("`id` section missing")
        if 'active' not in obj:
            raise ValueError("`active` section missing")
        if 'data_source' not in obj:
            raise ValueError("`data_source` section missing")
        if obj['data_source'] not in self.valid_data_sources:
            raise ValueError("Using unrecognized data source!")
