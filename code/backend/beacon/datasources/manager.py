# -*- coding: utf-8 -*-

import copy
import logging

from typing import List

from beacon.helpers import load_yaml_file
from beacon.connectors.adapters import AbstractConnector
from .validators import DataSourcesValidator

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class DataSourcesManager:
    def __init__(self, connectors: dict=None):
        self.sources = {}
        self.connectors = {} if connectors is None else connectors
        self.data_sources = {}
        self.datasets = []
        self.declared_connectors = []
        self.unparsed_structure = {}
        self.connected = False
        self.validator = DataSourcesValidator()

    def reset(self):
        self.disconnect_data_sources()
        self.sources = {}
        self.data_sources = {}
        self.declared_connectors = []
        self.unparsed_structure = {}

    def disconnect_data_sources(self):
        if not self.connected:
            return
        for data_source in self.data_sources:
            data_source.disconnect()
        self.connected = False

    def load_data_sources_information_from_yaml_file(self, path_to_yaml_file: str):
        self.reset()
        self.unparsed_structure = load_yaml_file(path_to_yaml_file)
        self.validator.validate(self.unparsed_structure)
        self.initialize_connectors(self.unparsed_structure.get('connectors', []))
        self.initialize_data_sources(self.unparsed_structure.get('data_sources', []))
        self.initialize_datasets(self.unparsed_structure.get('datasets', []))

    def initialize_connectors(self, connectors: List[dict]):
        for connector in connectors:
            self.initialize_connector(connector)

    def initialize_connector(self, connector: dict):
        self.declared_connectors.append(connector)

    def initialize_data_sources(self, data_sources: List[dict]):
        for data_source in data_sources:
            self.initialize_data_source(data_source)
        self.connected = True

    def initialize_data_source(self, data_source: dict):
        new_data_source_id = data_source.get('id')
        new_data_source_type = data_source.get('type')
        new_data_source_params = data_source.get('params')
        if new_data_source_type not in self.declared_connectors:
            message = f"Using unrecognised connector in: {new_data_source_id}."
            message += "It must appear in `connections.yaml`"
            raise ValueError(message)
        if new_data_source_type not in self.connectors:
            message = f"Using unloaded connector in: {new_data_source_id}."
            message += "It must be valid class in `adapters` directory."
            message += "Maybe you initialized this class without parameters?"
            raise ValueError(message)
        self.data_sources[new_data_source_id] = self.instantiate_connector(
            self.connectors.get(new_data_source_type),
            new_data_source_params
        )

    @staticmethod
    def instantiate_connector(connector_class: AbstractConnector, parameters: dict):
        new_connector = connector_class(parameters)
        new_connector.connect()
        return new_connector

    def initialize_datasets(self, datasets: List[dict]):
        for dataset in datasets:
            self.initialize_dataset(dataset)

    def initialize_dataset(self, dataset: dict):
        active = dataset.get('active', False)
        if active:
            self.datasets.append(dataset)

    def does_dataset_exist(self, dataset_id: int) -> bool:
        for dataset in self.datasets:
            if dataset_id == dataset.get('id'):
                return True
        return False

    def get_dataset_by_id(self, dataset_id: int):
        for dataset in self.datasets:
            if dataset_id == dataset.get('id'):
                return dataset
        msg = "Dataset with id=" + dataset_id + " has not been found."
        raise KeyError(msg)

    def get_datasource_by_id(self, datasource_id: str):
        for data_source in self.data_sources.keys():
            if datasource_id == data_source:
                return self.data_sources[data_source]
        msg = "Data source with id=" + datasource_id + " has not been found."
        raise KeyError(msg)

    def get_datasource_for_dataset(self, dataset_id: int):
        dataset = self.get_dataset_by_id(dataset_id)
        datasource_id = dataset.get('data_source')
        return self.get_datasource_by_id(datasource_id)

    def is_dataset_accessible_by_user(self, dataset_id: int, user_groups: List[str]):
        dataset = self.get_dataset_by_id(dataset_id)
        if self.is_dataset_public(dataset):
            return True
        return self.does_user_have_permissions_to_dataset(dataset, user_groups)

    def is_dataset_public_by_id(self, dataset_id: int):
        dataset_information = self.get_dataset_by_id(dataset_id)
        return 'protected' not in dataset_information

    @staticmethod
    def is_dataset_public(dataset_information: dict):
        return 'protected' not in dataset_information

    @staticmethod
    def does_user_have_permissions_to_dataset(dataset_information: dict, user_groups: List[str]):
        if dataset_information['protected'] == 'aai':
            required_group_name = dataset_information.get('aaiResourceName')
            return required_group_name in user_groups
        return False

    def update_authorization_info(self, beacon_info: dict, user_groups: List[str]):
        info = copy.deepcopy(beacon_info)
        datasets_from_manifest = info.get('datasets', [])
        info['datasets'] = []
        for dataset in datasets_from_manifest:
            if self.does_dataset_exist(dataset.get('id', -1)):
                self.update_authorization_info_for_dataset(dataset, user_groups)
                info['datasets'].append(dataset)
        return info

    def update_authorization_info_for_dataset(self, dataset: dict, user_groups: List[str]):
        try:
            dataset_id = dataset.get('id', -1)
            dataset_information = self.get_dataset_by_id(dataset_id)
            if self.is_dataset_public(dataset_information):
                dataset['info']['authorized'] = True
                dataset['info']['accessType'] = "PUBLIC"
            else:
                self.handle_protection(dataset, user_groups, dataset_information)
        except KeyError:
            message = "Manifest of Beacon differs (beacon_information.yaml) "
            message += "from dataset declaration (connections.yaml). "
            message += "Dataset with ID=" + str(dataset_id)
            logger.debug(message)

    def handle_protection(self, dataset: dict, user_groups: List[str], dataset_information: dict):
        dataset['info']['accessType'] = "CONTROLLED"
        dataset['info']['authorized'] = self.does_user_have_permissions_to_dataset(dataset_information, user_groups)
