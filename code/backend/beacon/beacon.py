import logging
import os

from typing import List

from beacon.connectors.manager import ConnectorsManager
from beacon.data_types import create_beacon_allele_response_ok
from beacon.data_types import create_beacon_allele_response_error
from beacon.data_types import create_beacon_error
from beacon.data_types import create_beacon_dataset_allele_response_ok
from beacon.data_types import validate_input
from beacon.datasources.manager import DataSourcesManager
from beacon.settings.settings import Settings


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Beacon(object):
    def __init__(self):
        self.connectors_manager = ConnectorsManager()
        self.connectors_manager.discover_and_load_adapters()
        self.connectors = self.connectors_manager.connectors
        self.datasources_manager = DataSourcesManager(self.connectors)
        self.settings = Settings()

    def get_information_for_user(self, user_groups: List[str]):
        info = self.settings.get_beacon_information()
        info_for_user = self.datasources_manager.update_authorization_info(info, user_groups)
        return info_for_user

    def shutdown(self, signum, frame):
        """
        Closes the opened connections.
        Warning - closes only stateful connections, it does nothing to stateless ones.
        For the optional parameters, see https://docs.python.org/3.9/library/signal.html#example
        """
        logger.debug(' Received SIGINT or SIGTERM signal {signum}, disconnecting stateful data sources')
        self.datasources_manager.disconnect_data_sources()

    def handle_include_dataset_responses(self, response: List[dict], responses, flag: str):
        flag = flag.upper()

        if len(flag) == 0 or flag == "NONE":
            return

        if flag != "HIT" and flag != "MISS" and flag != "ALL":
            return

        response['datasetAlleleResponses'] = []
        for r in responses:
            exists = bool(r[1])
            if flag == "ALL" or (flag == "MISS" and not exists) or (flag == "HIT" and exists):
                allele_response = create_beacon_dataset_allele_response_ok(datasetId=r[0], exists=exists)
                response['datasetAlleleResponses'].append(allele_response)

    def dispatch_query(self, query: dict, user_groups: List[str]):
        beacon_id = self.settings.get_beacon_information().get('id')
        dataset_ids = query.get('datasetIds', [])
        if not isinstance(dataset_ids, list):
            dataset_ids = [dataset_ids]

        try:
            validate_input(**query)

            # First, get all the relevant datasets.
            # If the user didn't specify which ones - then get them all
            if len(dataset_ids) == 0:
                all_datasets = self.datasources_manager.datasets
                dataset_ids = [dataset.get('id') for dataset in all_datasets]

            # Then build queries for each dataset, pass them to data sources
            # and collect the responses
            responses = []
            for dataset_id in dataset_ids:
                result = False
                if self.datasources_manager.is_dataset_accessible_by_user(dataset_id, user_groups):
                    data_source = self.datasources_manager.get_datasource_for_dataset(dataset_id)
                    backend_response = data_source.query(query)
                    result = data_source.translate_response(backend_response)
                responses.append((dataset_id, result))

            response = create_beacon_allele_response_ok(
                exists=any(resp[1] for resp in responses),
                beaconId=beacon_id,
                alleleRequest=query
            )

            self.handle_include_dataset_responses(response,
                                                  responses,
                                                  query.get('includeDatasetResponses', ""))

        except (NotImplementedError, Exception) as error:
            response = create_beacon_allele_response_error(
                beaconId=beacon_id,
                error=create_beacon_error(message=str(error.args))
            )

        return response

    def initialize_from_envvars(self,
                                settings_envvar: str='BEACON_SETTINGS_YAML',
                                datasources_envvar: str='BEACON_DATASOURCES_YAML'):
        logger.debug("    * Trying to load the settings from: $" + settings_envvar + ", $" + datasources_envvar)
        if settings_envvar not in os.environ or datasources_envvar not in os.environ:
            return False
        path_to_settings_yaml_file = os.environ.get(settings_envvar)
        path_to_datasources_yaml_file = os.environ.get(datasources_envvar)
        return self.initialize_from_yaml_files(
            path_to_settings_yaml_file,
            path_to_datasources_yaml_file
        )

    def initialize_from_yaml_files(self,
                                   path_to_settings_yaml_file: str,
                                   path_to_datasources_yaml_file: str):
        def _do_initialize():
            logger.debug("    * Initializing the Beacon with:")
            logger.debug("      [settings=\"" + path_to_settings_yaml_file + "\"], ")
            logger.debug("      [datasources=\"" + path_to_datasources_yaml_file + "\"]")
            self.settings.load_beacon_information_from_yaml_file(
                path_to_settings_yaml_file
            )
            self.datasources_manager.load_data_sources_information_from_yaml_file(
                path_to_datasources_yaml_file
            )
            return True

        try:
            return _do_initialize()
        except Exception as e:
            message = '    * Could not initialize Beacon with yaml files: \"'
            message = message + str(e) + "\""
            logger.error(message)
            return False
