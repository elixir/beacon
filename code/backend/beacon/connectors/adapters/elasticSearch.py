# -*- coding: utf-8 -*-

import json
import re

from .abstract import AbstractConnector
from beacon.helpers import make_http_post_request


class ElasticSearchConnector(AbstractConnector):
    """
    VariantDB connector.
    """

    def __init__(self, params: dict):
        self._params = params

    @staticmethod
    def get_id():
        """
        Identifier of this connector
        :return:
        """
        return "elastic_search"

    def connect(self):
        """
        No need to connect, as VariantDB is stateless and relies on HTTP
        requests passing
        """
        pass

    def disconnect(self):
        """
        No need to disconnect, as VariantDB is stateless and relies on HTTP
        requests passing
        """
        pass

    def build_query(self, query_parameters: dict):
        """
        Translates query to URL (as communication with VariantDB's goes
        through HTTP.
        :param query_parameters: parameters from the user
        :return: the URL to visit to get the results
        """

        def build_contig(contig):
            contig = contig.lower()
            if contig == 'mt':
                contig = 'm'
            return {
                'constant_score': {
                    'filter': {
                        'term': {
                            'contig': 'chr' + contig
                        }
                    }
                }
            }

        def build_start(start):
            return {
                'constant_score': {
                    'filter': {
                        'term': {
                            'pos': start
                        }
                    }
                }
            }

        def build_end(end):
            return {
                'constant_score': {
                    'filter': {
                        'script': {
                            'script': "_source.ref.size() + _source.pos = " + end
                        }
                    }
                }
            }

        def build_start_ranges(start_min, start_max):
            val = {
                'constant_score': {
                    'range': {
                        'pos': {
                            "gte": start_min,
                            "lte": start_max
                        },
                    }
                }
            }
            return val

        def build_end_ranges(end_min, end_max):
            return {
                'constant_score': {
                    'filter': {
                        'script': {
                            'script': "_source.ref.size() + _source.pos > " + end_min,
                            'script': "_source.ref.size() + _source.pos < " + end_max
                        }
                    }
                }
            }

        def build_ref(ref):
            return {
                'constant_score': {
                    'filter': {
                        'term': {
                            'ref': ref
                        }
                    }
                }
            }

        def build_alt(alt):
            return {
                'constant_score': {
                    'filter': {
                        'term': {
                            'alt': alt
                        }
                    }
                }
            }

        # Base URL to the elastic search
        url = self._params.get('endpoint')

        # Query filters
        filters = []

        # `assemblyId` [BeaconAPI name] => none [VariantDB uses only GRCh37]
        if re.search('ncbi36',
                     query_parameters['assemblyId'],
                     re.IGNORECASE):
            pass
            # Note: Not implemented yet
            # query_parameters = do_lift_over(query_parameters)

        if re.search('grch38',
                     query_parameters['assemblyId'],
                     re.IGNORECASE):
            pass
            # Note: Not implemented, too
            # query_parameters = do_lift_over(query_parameters)

        filters.append(build_contig(query_parameters['referenceName']))

        # `start` [BeaconAPI name] == `start` [VariantDB name]
        if 'start' in query_parameters and len(query_parameters['start']):
            filters.append(build_start(query_parameters['start']))

        # `end` [BeaconAPI name] == `end` [VariantDB name]
        if 'end' in query_parameters and len(query_parameters['end']):
            filters.append(build_end(query_parameters['end']))

        if 'startMin' in query_parameters and len(query_parameters['startMin']) or \
                'startMax' in query_parameters and len(query_parameters['startMax']):
            filters.append(build_start_ranges(
                query_parameters.get('startMin', -1),
                query_parameters.get('startMax', -1))
            )

        if 'endMin' in query_parameters and len(query_parameters['endMin']) or \
                'endMax' in query_parameters and len(query_parameters['endMax']):
            filters.append(build_end_ranges(
                query_parameters.get('endMin', -1),
                query_parameters.get('endMax', -1))
            )

        # referenceBases [BeaconAPI name] == ref [VariantDB name]
        if 'referenceBases' in query_parameters and len(query_parameters['referenceBases']):
            filters.append(build_ref(query_parameters['referenceBases']))

        # alternateBases [BeaconAPI name] == alleleseq [VariantDB name]
        if 'alternateBases' in query_parameters and len(query_parameters['alternateBases']):
            filters.append(build_alt(query_parameters['alternateBases']))

        query_dict = {
            '_source': ['contig', 'pos', 'ref', 'alt'],
            'query': {
                'bool': {
                    'filter': filters
                }
            }
        }

        return url, query_dict

    def query(self, query_parameters: dict):
        query_url, query_data = self.build_query(query_parameters)
        return make_http_post_request(query_url, query_data)

    def translate_response(self, response: str):
        if "Unauthorized" in response:
            return False
        elastic_response = json.loads(response)
        if "hits" in elastic_response and "total" in elastic_response.get("hits"):
            return elastic_response.get("hits").get("total") > 0
