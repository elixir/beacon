# -*- coding: utf-8 -*-

from .abstract import AbstractConnector


class MockConnector(AbstractConnector):
    """
    Mock connector.
    """

    def __init__(self, params: dict):
        self._params = params

    @staticmethod
    def get_id() -> str:
        """
        Identifier of this connector
        :return:
        """
        return "mock"

    def connect(self):
        """No need to connect"""
        pass

    def disconnect(self):
        """No need to disconnect"""
        pass

    def build_query(self, _: dict) -> dict:
        """No need to do anything"""
        return {}

    def query(self, query_parameters: dict) -> dict:
        """No need to query, just return an empty dict"""
        query_dict = self.build_query(query_parameters)
        return query_dict

    def translate_response(self, response: str) -> bool:
        """Will always return True"""
        return True
