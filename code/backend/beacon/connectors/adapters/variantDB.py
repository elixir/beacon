# -*- coding: utf-8 -*-

import json
import re

from .abstract import AbstractConnector
from beacon.helpers import make_http_request


class VariantDBConnector(AbstractConnector):
    """
    VariantDB connector.
    """

    def __init__(self, params: dict):
        self._params = params

    @staticmethod
    def get_id():
        """
        Identifier of this connector
        :return:
        """
        return "variant_db"

    def connect(self):
        """
        No need to connect, as VariantDB is stateless and relies on HTTP
        requests passing
        """
        pass

    def disconnect(self):
        """
        No need to disconnect, as VariantDB is stateless and relies on HTTP
        requests passing
        """
        pass

    def build_query(self, query_parameters: dict):
        """
        Translates query to URL (as communication with VariantDB's goes
        through HTTP.
        :param query_parameters: parameters from the user
        :return: the URL to visit to get the results
        """
        # Base URL to the VariantDB, with parameters to be appended
        url = u'/variants/getfields!eq!start,reference,alleleseq&count!eq!t'

        # `assemblyId` [BeaconAPI name] => none [VariantDB uses only GRCh37]
        if re.search('ncbi36',
                     query_parameters['assemblyId'],
                     re.IGNORECASE):
            pass
            # Note: Not implemented yet
            # query_parameters = do_lift_over(query_parameters)

        if re.search('grch38',
                     query_parameters['assemblyId'],
                     re.IGNORECASE):
            pass
            # Note: Not implemented, too
            # query_parameters = do_lift_over(query_parameters)

        # VariantDB uses lower-case letters - so we need to convert it
        query_parameters['referenceName'] = \
            query_parameters['referenceName'].lower()

        # VariantDB stores MT as `m` - so we need to convert it
        if query_parameters['referenceName'] == 'mt':
            query_parameters['referenceName'] = 'm'

        # `referenceName` [BeaconAPI name] == `chrom` [VariantDB name]
        url = url + '&chrom!eq!' + query_parameters['referenceName']

        # `start` [BeaconAPI name] == `start` [VariantDB name]
        if 'start' in query_parameters and len(query_parameters['start']):
            url = url + '&start!eq!' + query_parameters['start']

        # `end` [BeaconAPI name] == `end` [VariantDB name]
        if 'end' in query_parameters and len(query_parameters['end']):
            url = url + "&end!eq!" + query_parameters['end']

        range_queries = {
            'startMin': False,
            'startMax': False,
            'endMin': False,
            'endMax': False
        }
        if 'startMin' in query_parameters and len(query_parameters['startMin']):
            range_queries['startMin'] = True
            url = url + '&start!gt!' + query_parameters['startMin']
        if 'startMax' in query_parameters and len(query_parameters['startMax']):
            range_queries['startMax'] = True
            url = url + '&start!lt!' + query_parameters['startMax']
        if 'endMin' in query_parameters and len(query_parameters['endMin']):
            range_queries['endMin'] = True
            url = url + '&end!gt!' + query_parameters['endMin']
        if 'endMax' in query_parameters and len(query_parameters['endMax']):
            range_queries['endMax'] = True
            url = url + '&end!lt!' + query_parameters['endMax']
        # If the range is unclosed, we need to close it...
        if range_queries['endMax'] + range_queries['endMin'] == 1:
            if range_queries['endMax']:
                url = url + '&end!gt!0'
            if range_queries['endMin']:
                url = url + '&end!lt!10000000000'
        if range_queries['startMax'] + range_queries['startMin'] == 1:
            if range_queries['startMax']:
                url = url + '&start!gt!0'
            if range_queries['startMin']:
                url = url + '&start!lt!10000000000'

        # referenceBases [BeaconAPI name] == ref [VariantDB name]
        if 'referenceBases' in query_parameters and len(query_parameters['referenceBases']):
            url = url + "&ref!eq!" + query_parameters['referenceBases']

        # alternateBases [BeaconAPI name] == alleleseq [VariantDB name]
        if 'alternateBases' in query_parameters and len(query_parameters['alternateBases']):
            url = url + "&alleleseq!eq!" + query_parameters['alternateBases']

        return self._params.get('endpoint') + url

    def query(self, query_parameters: dict):
        query_url = self.build_query(query_parameters)
        return make_http_request(query_url)

    def translate_response(self, response: str):
        response_dict = json.loads(response)
        if "values" not in response_dict or len(response_dict.get('values')) != 1:
            raise Exception("VariantDB returns invalid response")
        responses = response_dict.get("values", [0])
        return int(responses[0]) > 0

    def test_connection(self):
        self._params.get('endpoint')
