# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod


class AbstractConnector(ABC):
    def __init__(self, params: dict):
        self._params = params
        pass

    @staticmethod
    @abstractmethod
    def get_id():
        """
        Should return an identifier of the connector.
        This ID will be used in configuration file.
        :return: ID, string
        """
        pass

    def query(self, query_parameters: dict):
        """
        Should pass the query into the data source and receive the result
        :param query_parameters: the query made by the user
        :return: the data received by the data source
        """
        the_query = self.build_query(query_parameters)
        the_response = self.query(the_query)

        return self.translate_response(the_response)

    # For stateful data sources =============================================

    @abstractmethod
    def connect(self):
        """
        Should establish a connection to a data source.
        :return: does not return anything
        """
        pass

    @abstractmethod
    def disconnect(self):
        """
        Should close a connection to a data source.
        :return: does not return anything
        """
        pass

    # For all data sources ==================================================

    @abstractmethod
    def build_query(self, query_parameters: dict):
        """
        Should translate the query into a form that is understandable
        by the data source.
        :param query_parameters: the query made by the user
        :return: the query understandable by the data source
        """

    @abstractmethod
    def translate_response(self, response: dict):
        """
        Should translate the response received from the data source,
        into a form compliant to Beacon API
        :param response: response received from the data source
        :return: response that should be propagated to the user
                 as the response for his query
        """
        pass



