#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import inspect
import logging

from beacon.connectors import adapters


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ConnectorsManager:
    def __init__(self):
        self.connectors = {}
        self.connectors = self.discover_and_load_adapters()

    @staticmethod
    def is_adapter(class_name: str, class_class):
        # We don't want to include Python built-ins
        if class_name.startswith('_'):
            return False
        # We don't want to include submodules
        if class_class.__class__.__name__ == "module":
            return False
        # We want only classes that extend AbstractConnector
        if not class_class.__base__.__name__ == 'AbstractConnector':
            return False
        # We don't want AbstractConnector itself
        if class_class.__name__ == 'AbstractConnector':
            return False
        return True

    def discover_and_load_adapters(self):
        """
        Loads the adapter classes from the `adapters` folder
        """
        connector_classes = {}
        modules = inspect.getmembers(adapters)
        for (module_name, module_class) in modules:
            if not self.is_adapter(module_name, module_class):
                continue
            try:
                name = "{not loaded}"
                module_id = module_class.get_id()
                name = module_class.__class__.__name__
                connector_classes[module_id] = module_class
            except Exception as e:
                message = f"There was an error while loading a connector {name}: {str(e)}"
                logger.debug(message)
        return connector_classes
