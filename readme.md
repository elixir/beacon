<img src="code/frontend/images/elixir_logo_big.png" alt="elixir-lu" width="200" />

# Note:
This branch implements the Beacon API 1.0. It is not finished, thus this _readme_ is not updated yet.

# ELIXIR Beacon 
[![pipeline status](https://gitlab.lcsb.uni.lu/elixir/elixir-beacon/badges/master/pipeline.svg)](https://gitlab.lcsb.uni.lu/elixir/elixir-beacon/commits/master)[![coverage report](https://gitlab.lcsb.uni.lu/elixir/elixir-beacon/badges/master/coverage.svg)](https://gitlab.lcsb.uni.lu/elixir/elixir-beacon/commits/master)

This is ELIXIR Project's Beacon implementation in Python. 

It was developed by ELIXIR-LU (ELIXIR Luxembourg) and serves as a code base for the ELIXIR-LU Beacon deployment.

It is licensed under Apache2 license (see `LICENSE` file).
It is intended to work with separate databases of variants.
Moreover, it is capable of restricting access to datasets, utilizing _Elixir AAI_.

## Back-ends
The Beacon acts as a middleware, which passes the requests to the defined back-ends, and collects the response.
Currently, it supports VariantDB (in-house solution to host and query genetic variants), and _Elasticsearch_.
It could be also used as a framework to connect even different back-end types. Extending the capabilities requires only adding adapters to connect to other variant storage; see the relevant code in `./code/backend/beacon/connectors/adapters` directory.

## Deployment instructions
1. Install Docker and Docker-compose.
2. Clone this very repository.
3. Copy `configuration/templates/flask_settings.json` into `configuration/flask_settings.json` and modify at least `SECRET_KEY`.
If you don't do it, other people could be able to forge session information.
4. Copy `configuration/templates/beacon_information.yaml` into `configuration/beacon_information.yaml`. This file contains the information about Beacon and datasets that it contains.
5. Copy `configuration/templates/connections.yaml` into `configuration/connections.yaml`. This file describes how to connect to the back-ends that host datasets' information.
6. If you want to use _OpenID Connect_ to connect to `ELIXIR-AAI`, copy `configuration/templates/client_secrets.json` into `configuration/client_secrets.json` and modify it accordingly.
What is important, that under `redirect_uris` key, you should put your domain appended with `/oidc_callback`. 
(That endpoint is automatically added to the server by _Flask-OIDC_).
7. Update `server_name 192.168.99.100 beacon-dev-elixir-luxembourg.lcsb.uni.lu;` line (to match your server's IP) and 
 `proxy_pass http://192.168.99.100:8123;` (to match it's local IP) in `configuration/deploy/beacon.conf`, so that the nginx is configured properly.
 Setting up proper HTTPS connection requires generating `nginx-selfsigned*` files and `dhparam.pem` (if you don't know how to do it, follow any nginx-related tutorial). 
8. Use `docker-compose up --build -d` to run a daemon using docker. Consult `Dockerfile` and `docker-compose.yml` files.

## Analytics
We use Matomo in our applications, with aggregated users' information - no personal information is stored. By default, **it is turned off**. However, if you have Matomo instance running and would like to use it as well, just tune `code/frontend/templates/index.html` - in head section and in footer.

 
## References
 * [API specification](https://github.com/ga4gh/beacon-team/blob/develop/src/main/resources/avro/beacon.avdl)
 * [Beacon Network](https://beacon-network.org//#/)
 * [API Reference](https://beacon-network.org//#/developers/api/beacon-network)
 * [Example Beacon implementation](https://github.com/elixirhub/human-data-beacon)
 * [ELIXIR Beacon page](https://genomicsandhealth.org/work-products-demonstration-projects/beacon-project-0)
 * [ELIXIR Beacon information](https://www.elixir-europe.org/about-us/implementation-studies/beacons)
 * [ELIXIR Beacon API reference](https://github.com/elixirhub/human-data-beacon#using-the-application)
 * [ELIXIR page](https://www.elixir-europe.org/)


## Thanks
Thanks to Miro Cupak and his [Python Beacon Development Kit (PBDK)](https://github.com/mcupak/beacon-python/), which helped greatly in establishing the first version of ELIXIR-LU's Beacon.

## Contact/issues etc.
To request new feature or report a bug you can either use [gitlab's issue tracker](https://gitlab.lcsb.uni.lu/elixir/elixir-beacon/issues), or send an email to `jacek.lebioda@uni.lu`.
